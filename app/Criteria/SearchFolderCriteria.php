<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Http\Requests\dossierSearchRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\DateUtils;

/**
 * Class SearchFolderCriteria.
 *
 * @package namespace App\Criteria;
 */
class SearchFolderCriteria implements CriteriaInterface
{
    protected $resquest;
    public function __construct(dossierSearchRequest $request)
    {   
        $this->request=$request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        
        if($this->request->filled('mots')){
            $model = $model->where('objet','like', '%'.$this->request->get('mots').'%' )
            ->orWhere('description', 'like', '%'.$this->request->get('mots').'%');
        }
        if($this->request->filled('division_id')){
            $model = $model->where('division_id','=',$this->request->get('division_id') );
        }
        if($this->request->filled('type_id')){
            $model = $model->where('type_id','=',$this->request->get('type_id') );
        }
        if($this->request->filled('division_id')){
            $model = $model->where('division_id','=',$this->request->get('division_id') );
        }
        if($this->request->filled('date_debut')){
            $model = $model->whereDate('date_debut','>=',DateUtils::stringToDate($this->request->get('date_debut')));
        }

        if($this->request->filled('date_fin')){
            $model = $model->whereDate('date_fin','<=',DateUtils::stringToDate($this->request->get('date_fin')));
        }
        return $model;
    }
}
