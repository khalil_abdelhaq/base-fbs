<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Http\Requests\MpSearchRequest;
use Illuminate\Support\Facades\Auth;
use App\Utils\DateUtils;

/**
 * Class SearchFolderCriteria.
 *
 * @package namespace App\Criteria;
 */
class SearchMpCriteria implements CriteriaInterface
{
    protected $resquest;
    public function __construct(MpSearchRequest $request)
    {   
        $this->request=$request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->filled('division_id')){
            $model = $model->where('division_id','=',$this->request->get('division_id') );
        }
      
        if($this->request->filled('date_fin')){
            $model = $model->whereDate('date_limit','=',DateUtils::stringToDate($this->request->get('date_limit')));
        }
        if($this->request->filled('etat')){
            $model = $model->where('etat','=',$this->request->get('etat') );
        }
        if($this->request->filled('type')){
            $model = $model->where('type','=',$this->request->get('type') );
        }
        return $model;
    }
}
