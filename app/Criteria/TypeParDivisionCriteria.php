<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Support\Facades\Auth;

/**
 * Class DossierParUserCriteria.
 *
 * @package namespace App\Criteria;
 */
class TypeParDivisionCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {   
        if(Auth::user()->isSuperAdmin()){
        return $model;
    }
    else{
        $model = $model->where('division_id','=', Auth::user()->division_id );
        return $model;   
    }
}
}