<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\DossierRepository::class, \App\Repositories\DossierRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DivisionRepository::class, \App\Repositories\DivisionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EvenementRepository::class, \App\Repositories\EvenementRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EtatRepository::class, \App\Repositories\EtatRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TypedossierRepository::class, \App\Repositories\TypedossierRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MpRepository::class, \App\Repositories\MpRepositoryEloquent::class);
        //:end-bindings:
    }
}
