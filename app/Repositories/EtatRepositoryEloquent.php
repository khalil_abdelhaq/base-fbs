<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\etatRepository;
use App\Entities\Etat;
use App\Validators\EtatValidator;

/**
 * Class EtatRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EtatRepositoryEloquent extends BaseRepository implements EtatRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Etat::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
