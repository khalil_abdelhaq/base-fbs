<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EtatRepository.
 *
 * @package namespace App\Repositories;
 */
interface EtatRepository extends RepositoryInterface
{
    //
}
