<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\dossierRepository;
use App\Entities\Dossier;
use App\Validators\DossierValidator;

/**
 * Class DossierRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class DossierRepositoryEloquent extends BaseRepository implements DossierRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Dossier::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
