<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DossierRepository.
 *
 * @package namespace App\Repositories;
 */
interface DossierRepository extends RepositoryInterface
{
    //
}
