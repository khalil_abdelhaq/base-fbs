<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\typedossierRepository;
use App\Entities\Typedossier;
use App\Validators\TypedossierValidator;

/**
 * Class TypedossierRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TypedossierRepositoryEloquent extends BaseRepository implements TypedossierRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Typedossier::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
