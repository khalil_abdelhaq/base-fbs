<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EvenementRepository.
 *
 * @package namespace App\Repositories;
 */
interface EvenementRepository extends RepositoryInterface
{
    //
}
