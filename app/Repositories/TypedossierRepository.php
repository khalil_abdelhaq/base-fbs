<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TypedossierRepository.
 *
 * @package namespace App\Repositories;
 */
interface TypedossierRepository extends RepositoryInterface
{
    //
}
