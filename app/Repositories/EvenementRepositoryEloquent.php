<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\evenementRepository;
use App\Entities\Evenement;
use App\Validators\EvenementValidator;

/**
 * Class EvenementRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EvenementRepositoryEloquent extends BaseRepository implements EvenementRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Evenement::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
