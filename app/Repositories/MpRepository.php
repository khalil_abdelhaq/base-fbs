<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MpRepository.
 *
 * @package namespace App\Repositories;
 */
interface MpRepository extends RepositoryInterface
{
    //
}
