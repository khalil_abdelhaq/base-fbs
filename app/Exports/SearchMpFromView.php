<?php

namespace App\Exports;

use App\Entities\Mp;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class SearchMpFromView implements FromView
{
     /**
    * @return \Illuminate\Support\View
    */
    public function view(): View
    {
        return view('layouts.mps.searchMpTable', [
            'mps' => Mp::all(),
        ]);
    }
}
