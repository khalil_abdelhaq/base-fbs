<?php

namespace App\Exports;

use App\Entities\Mp;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Repositories\MpRepository;

class MpsExport implements FromView
{
   
    /**
    * @return \Illuminate\Support\View
    */
    public function view(): View
    {
        return view('layouts.mps.mptable', [
            'mps' => Mp::all(),
        ]);
    }
}
