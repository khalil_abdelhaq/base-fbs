<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEvenement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'intitule' => 'required|max:255',
            'remarques' => 'required',

        ];
    }
    public function messages()
    {
        return [
            'intitule.required' => 'Veuillez remplir intitulé',
            'remarques.required' => 'Veuillez remplir remarques',
        ];
    }
}
