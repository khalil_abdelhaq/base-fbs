<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class dossierUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'objet' => 'required|max:255',
            'type_id' => 'required',
            'priorite' => 'required',
            'date_debut' => 'required|date',
            'date_fin' => 'required|date|after:date_debut',
            'description' => 'required',

        ];
    }
    public function messages()
{
    return [
        'objet.required' => 'Veuillez remplir intitulé',
        'type_id.required'  => 'Veuillez remplir type du dossier',
        'priorite.required' => 'veuillez remplir priorité',
        'date_fin.after' => 'la date fin doit être postérieur',
        'description.required' => 'veuillez remplir la description',
    ];
}

}
