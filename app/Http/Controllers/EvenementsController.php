<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EvenementRepository;
use  App\Http\Requests\CreateEvenement;
use App\Validators\EvenementValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class EvenementsController extends Controller
{
    protected $repository;
    protected $validator;
    public function __construct(EvenementRepository $repository,EvenementValidator $validator)
    {   
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function store(CreateEvenement $request)
    {
        try {

           $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $dossier = $this->repository->create($request->all());
           

            $response = [
                'message' => 'Dossier créé avec succèe.',
                'data'    => $dossier->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->back()->with('response', $response);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }
}
