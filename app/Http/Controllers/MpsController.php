<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MpCreateRequest;
use App\Http\Requests\MpUpdateRequest;
use App\Repositories\MpRepository;
use App\Validators\MpValidator;
use App\Repositories\DivisionRepository;
use App\Http\Requests\MpSearchRequest;
use App\Criteria\SearchMpCriteria;
use App\Exports\MpsExport;
use App\Exports\SearchMpFromView;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class MpsController.
 *
 * @package namespace App\Http\Controllers;
 */
class MpsController extends Controller
{
    /**
     * @var MpRepository
     */
    protected $repository;
    protected $divisionrepo;

    /**
     * @var MpValidator
     */
    protected $validator;

    /**
     * MpsController constructor.
     *
     * @param MpRepository $repository
     * @param MpValidator $validator
     */
    public function __construct(MpRepository $repository,DivisionRepository $division, MpValidator $validator)
    {
        $this->repository = $repository;
        $this->divisionrepo=$division;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $divisions =$this->divisionrepo->pluck('intitule','id');
        $mps = $this->repository->paginate(10);
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $mps,
            ]);
        }

        return view('layouts.mps.index', compact('mps','divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  MpCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(MpCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $mp = $this->repository->create($request->all());

            $response = [
                'message' => 'Mp crée avec succèe.',
                'data'    => $mp->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mp = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $mp,
            ]);
        }

        return view('mps.show', compact('mp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mp = $this->repository->find($id);
        $divisions =$this->divisionrepo->pluck('intitule','id');
        return view('layouts.mps.updatemps', compact('mp','divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MpUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(MpUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $mp = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Effectué avec succèe.',
                'data'    => $mp->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Mp deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Mp deleted.');
    }
    public function search(MpSearchRequest $request){
        $mps = $this->repository->pushCriteria(new SearchMpCriteria($request))->paginate(8);
        return redirect()->back()->with('response', $mps);
    }

    public function create(){
        $divisions = $this->divisionrepo->pluck('acronyme','id');
        return view('layouts.mps.addmps',compact('divisions'));
    }
    public function export() 
{
    return Excel::download(new MpsExport(), 'mps.xlsx');
}

public function exportsearch() 
{
    return Excel::download(new SearchMpFromView(), 'mps.xlsx');
}
}
