<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DossierCreateRequest;
use App\Http\Requests\DossierUpdateRequest;
use App\Repositories\DossierRepository;
use App\Repositories\DivisionRepository;
use App\Repositories\TypedossierRepository;
use App\Repositories\EtatRepository;
use App\Validators\DossierValidator;
use App\Criteria\DossierParUserCriteria;
use App\Criteria\TypeParDivisionCriteria;
use App\Http\Requests\dossierSearchRequest;
use App\Criteria\SearchFolderCriteria;
use Illuminate\Support\Facades\DB;

/**
 * Class DossiersController.
 *
 * @package namespace App\Http\Controllers;
 */
class DossiersController extends Controller
{
    /**
     * @var DossierRepository
     */
    protected $repository;
    protected $divisionrepo;
    protected $typedossierrepo;
    protected $etatdossier;

    /**
     * @var DossierValidator
     */
    protected $validator;

    /**
     * DossiersController constructor.
     *
     * @param DossierRepository $repository
     * @param DossierValidator $validator
     */
    public function __construct(DossierRepository $repository,DivisionRepository $division,TypedossierRepository $typedossier,EtatRepository $etatdossier,DossierValidator $validator)
    {   //$this->middleware('auth');
        $this->repository = $repository;
        $this->divisionrepo= $division;
        $this ->typedossierrepo= $typedossier;
        $this->etatdossier = $etatdossier;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $dossiers = $this->repository->with('divisionsimpliquees:acronyme','divisionconcernee:acronyme')->pushCriteria(DossierParUserCriteria::class)->paginate(8);
      if (request()->wantsJson()) {

           return response()->json([
             'data' => $dossiers,
          ]);
      }
       return view('layouts.index', compact('dossiers'));
    }
    public function create(){
        $divisions = $this->divisionrepo->pluck('acronyme','id');
        $types = $this->typedossierrepo->pushCriteria(TypeParDivisionCriteria::class)->pluck('intitule','id');
        return view('layouts.addFolder',compact('divisions','types'));
    }

    /**
     * Store a newly created resource in storage.
     *  
     * @param  DossierCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DossierCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $dossier = $this->repository->create($request->all());
            $dossier->divisionsimpliquees()->attach($request->get('di'));

            $response = [
                'message' => 'Dossier créé avec succèe.',
                'data'    => $dossier->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('response', $response);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $dossier = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $dossier,
            ]);
        }
        $evenements=$dossier->evenements;
        $divisionimpliques=$dossier->divisionsimpliquees;
         return view('layouts.detail_folder', compact('dossier','evenements','divisionimpliques'));       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dossier = $this->repository->find($id);
        $divisions = $this->divisionrepo->pluck('acronyme','id');
        $types = $this->typedossierrepo->pushCriteria(TypeParDivisionCriteria::class)->pluck('intitule','id');
        $etats =$this->etatdossier->pluck('intitule','id');
        return view('layouts.editFolder', compact('dossier','divisions','types','etats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DossierUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(DossierUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $dossier = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Effectué avec succèe.',
                'data'    => $dossier->toArray(),
            ];
            
            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {
                
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Dossier deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Dossier deleted.');
    }
    public function searchFolder(){
        $types = $this->typedossierrepo->pushCriteria(TypeParDivisionCriteria::class)->pluck('intitule','id');
        $divisions =$this->divisionrepo->pluck('intitule','id');
        return view('layouts.search',compact('types','divisions'));
    }
    public function search(dossierSearchRequest $request){
        $dossiers = $this->repository->pushCriteria(TypeParDivisionCriteria::class)->pushCriteria(new SearchFolderCriteria($request))->paginate(8);
        return redirect()->back()->with('response', $dossiers);
    }

    public function statistics(){
        $etatdossiers = DB::select('SELECT e.intitule,(select count(*) from dossiers d where d.etat_id = e.id) as count  FROM etats e WHERE 1 GROUP BY e.intitule,e.id');

        $divisions = DB::select('SELECT d.acronyme,(select count(*) from dossiers do where do.division_id = d.id) as count  FROM divisions d WHERE 1 GROUP BY d.acronyme,d.id');
        return view('layouts.statistics', compact('etatdossiers','divisions'));
    }
}
