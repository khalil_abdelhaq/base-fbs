<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Mp.
 *
 * @package namespace App\Entities;
 */
class Mp extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['num_envoi','date_envoie','num_arrive','date_arrive','emetteur','division_id','date_visa','type','objet','instructions','date_limit','etat','observations'];
    public $timestamps = false;

    public function divisionconcernee()
    {
        return $this->belongsTo('App\Entities\Division','division_id');
    }

}
