<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Evenement.
 *
 * @package namespace App\Entities;
 */
class Evenement extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['intitule','remarques','dossier_id'];
    public function etat()
    {
        return $this->belongsTo('App\Entities\Dossier');
    }

}
