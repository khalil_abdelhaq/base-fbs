<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Division.
 *
 * @package namespace App\Entities;
 */
class Division extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    public function dossiers()
    {
        return $this->hasMany('App\Entities\Dossier');
    }
    public function impliqueesdsdivisions()
    {
        return $this->belongsToMany('App\Entities\Dossier');
    }
}
