<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Dossier.
 *
 * @package namespace App\Entities;
 */
class Dossier extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['objet','description','priorite','date_debut','date_fin','division_id','etat_id','type_id'];
    public function divisionconcernee()
    {
        return $this->belongsTo('App\Entities\Division','division_id');
    }

    public function divisionsimpliquees()
    {
        return $this->belongsToMany('App\Entities\Division','dossier_concerne_division','dossier_id','division_id');
    }
    public function etat()
    {
        return $this->belongsTo('App\Entities\Etat');
    }
    public function type()
    {
        return $this->belongsTo('App\Entities\Typedossier');
    }
    public function evenements()
    {
        return $this->hasMany('App\Entities\Evenement');
    }
}
