<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::resource('dossiers','DossiersController');
Route::resource('evenements', 'EvenementsController');
Route::resource('mps','MpsController');
Route::get('/folder','DossiersController@index');
Route::post('dossier/search','DossiersController@search')->name('dossiers.search');
Route::post('mp/search','MpsController@search')->name('mps.search');
Route::get('/search','DossiersController@searchFolder');
Route::get('/statistics','DossiersController@statistics');
Auth::routes();


Route::get('/mps','MpsController@index');
Route::get('/export', 'MpsController@export')->name('mps.export');
Route::get('/exportsearch', 'MpsController@exportsearch')->name('mps.exportsearch');
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');


