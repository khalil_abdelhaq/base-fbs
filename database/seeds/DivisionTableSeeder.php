<?php

use Illuminate\Database\Seeder;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Division::create([
            'intitule' => 'Secrétariat Générale',
            'acronyme' => 'SG',
        ]);
        Division::create([
            'intitule' => 'Division affaires intérieures',
            'acronyme' => 'DAI',
        ]);
        Division::create([
            'intitule' => 'Division affaires rurales',
            'acronyme' => 'DAR',
        ]);
        Division::create([
            'intitule' => 'Division action sociale',
            'acronyme' => 'DAS',
        ]);
        Division::create([
            'intitule' => 'Division affaire économique et la coordination',
            'acronyme' => 'DAEC',
        ]);
        Division::create([
            'intitule' => 'Division Budget et Marché',
            'acronyme' => 'DBM',
        ]);
        Division::create([
            'intitule' => 'Division des ressources humaines et moyens généraux',
            'acronyme' => 'DRHMG',
        ]);
    }
}
