<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert([
            'intitule' => 'Secrétariat Génarale',
            'acronyme' => 'SG',
        ]);

        DB::table('divisions')->insert([
            'intitule' => 'Division Affaires Intérieures',
            'acronyme' => 'DAI',
        ]);

        DB::table('divisions')->insert([
            'intitule' => 'Division de l\'Action Sociale',
            'acronyme' => 'DAS',
        ]);

        DB::table('divisions')->insert([
            'intitule' => 'Division des Affaires Rurales',
            'acronyme' => 'DAR',
        ]);

        DB::table('divisions')->insert([
            'intitule' => 'Division Urbanisme et Environnement',
            'acronyme' => 'DUE',
        ]);

        DB::table('divisions')->insert([
            'intitule' => 'Division Technique',
            'acronyme' => 'DT',
        ]);

        DB::table('divisions')->insert([
            'intitule' => 'Division des Ressources Humaines et Moyens Généraux',
            'acronyme' => 'DRHMG',
        ]);
        
        DB::table('divisions')->insert([
            'intitule' => 'Division Budget et Marché',
            'acronyme' => 'DBM',
        ]);
        // $this->call(UsersTableSeeder::class);
    }
}
