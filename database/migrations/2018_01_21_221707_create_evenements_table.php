<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEvenementsTable.
 */
class CreateEvenementsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('evenements', function(Blueprint $table) {
            $table->increments('id');
			$table->string('intitule');
			$table->string('remarques');
			$table->integer('dossier_id')->unsigned();
            $table->timestamps();
		});
		Schema::table('evenements',function (Blueprint $table){
                $table->foreign('dossier_id')->references('id')->on('dossiers')->onDelete('cascade')
				->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('evenements');
	}
}
