<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_envoi')->nullable();
            $table->date('date_envoie')->nullable();
            $table->integer('num_arrive');
            $table->date('date_arrive');
            $table->string('emetteur');

            $table->integer('division_id')->unsigned();
            $table->date('date_visa');
            $table->string('type')->default('Instruction');
            $table->string('objet');
            $table->string('instructions');
            $table->string('etat')->default("en cours");;
            $table->string('observations')->nullable();
            $table->date('date_limit');
            $table->boolean('executed')->default(false);;
            $table->date('date_execution')->nullable();;
        });
        Schema::table('mps',function (Blueprint $table){
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade')
            ->onUpdate('cascade');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mps');
    }
}
