<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDossiersTable.
 */
class CreateDossiersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dossiers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('objet');
			$table->string('priorite');
			$table->string('description');
			$table->date('date_debut');
			$table->date('date_fin');
			$table->integer('division_id')->unsigned();
			$table->integer('etat_id')->unsigned()->default(1);
			$table->integer('type_id')->unsigned();
            $table->timestamps();
		});
		Schema::table('dossiers',function (Blueprint $table){
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade')
				->onUpdate('cascade');
				$table->foreign('etat_id')->references('id')->on('etats')->onDelete('cascade')
				->onUpdate('cascade');
				$table->foreign('type_id')->references('id')->on('typedossiers')->onDelete('cascade')
                ->onUpdate('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('dossiers');
	}
}
