<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTypedossiersTable.
 */
class CreateTypedossiersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('typedossiers', function(Blueprint $table) {
            $table->increments('id');
			$table->string('intitule');
			$table->integer('division_id')->unsigned();
            $table->timestamps();
		});
		Schema::table('typedossiers',function (Blueprint $table){
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade')
				->onUpdate('cascade');
        });
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('typedossiers');
	}
}
