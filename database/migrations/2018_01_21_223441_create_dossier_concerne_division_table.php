<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDossierConcerneDivisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dossier_concerne_division', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('dossier_id')->unsigned();
            $table->integer('division_id')->unsigned();
        });
        Schema::table('dossier_concerne_division',function (Blueprint $table){
            $table->foreign('division_id')->references('id')->on('divisions')->onDelete('cascade')
                ->onUpdate('cascade');
                $table->foreign('dossier_id')->references('id')->on('dossiers')->onDelete('cascade')
				->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dossier_concerne_division');
    }
}
