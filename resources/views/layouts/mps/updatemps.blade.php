@extends('layouts.app')
@section('content')               
                     
            <h3><span class="title">Edition MP</span><span class="underlined">&nbsp;</span></h3> 
                 @if (Session::get('message'))
                <div class="success">
                    {!! Session::get('message')!!}
                </div> 
                @endif
				{{ Form::model($mp, array('route' => array('mps.update', $mp->id), 'method' => 'PUT')) }}
                <fieldset class="new_folder">
                <legend>Référence bureau d'ordre</legend>
    			<table>
                    <tr>
                    <td>{{Form::label('num_envoi', 'Numéro d envoi')}}</td>
                    <td> {{Form::number('num_envoi')}}</td>
                    <td>{{Form::label('date_envoie', 'Date envoi')}}</td>
                    <td> {{Form::date('date_envoie')}}</td>
                    </tr>
                    <tr>
                    <td>{{Form::label('num_arrive', 'Numéro d arrivée')}}</td>
                    <td> {{Form::number('num_arrive')}}</td>
                    <td>{{Form::label('date_arrive', 'Date arrivée')}}</td>
                    <td> {{Form::date('date_arrive')}}</td>
                    </tr>

                    <tr>
                    <td>{{Form::label('emetteur', 'Emetteur')}}</td>
                    <td> {{Form::text('emetteur')}}</td>
                    </tr>
                </table>    
                </fieldset>
                
                <table>
                </table>
                <fieldset class="new_folder">
                <legend>Référence Cabinet</legend>
                <table>
                <tr>
                <td>{{Form::label('date_visa', 'Date visa')}}</td>
                <td> {{Form::date('date_visa')}}</td>
                </tr>
                <tr>
                <td>{{Form::label('division_id', 'Division')}} </td>
                <td>{!! Form::select('division_id', 
                            $divisions, 
                            null,['placeholder' => 'Selectionnez Division..']) !!}</td>
                </tr>
                <tr>
                <td>{{Form::label('objet', 'Objet')}}</td>
                <td> {{Form::text('objet')}}</td>
                </tr>
                <tr>
                <td>{{Form::label('instructions', 'Instructions')}}</td>
        		<td>{{ Form::textarea('instructions') }}</td>
                </tr>
                <tr>
                <td>{{Form::label('date_limit', 'Date limite')}}</td>
                <td> {{Form::date('date_limit')}}</td>
                </tr>
                </table>
                </fieldset>

                    {{Form::label('etat', 'Etat')}} 
                    {!! Form::select('etat', 
                            ["en cours" => "en cours","traité" => "traité"], 
                            null,[]) !!}</td>

               {{Form::label('observations', 'observations')}}
        		{{ Form::textarea('observations') }}

                {{Form::submit('Valider',array('class' => 'button'))}}
				{{ Form::close() }}
              


                <div class="clear"></div>
               
                @endsection