@extends('layouts.app')
@section('content')
<h3><span class="title">Les instructions en instance</span><span class="underlined">&nbsp;</span></h3>
              
<a class="add fa" id="addevenement" href="{{ route('mps.create') }}" title=""> Ajouter un élément</a>
               
                {{ Form::open(['route' => 'mps.search','class'=>'searchform']) }}
                <table>
                <tr>
                <td>{{Form::label('date_limit', 'Date limit')}}</td><td> {{Form::date('date_limit')}}</td>
                <td>{{Form::label('division_id', 'Division')}} </td><td>{!! Form::select('division_id', 
                            $divisions, 
                            null,['placeholder' => 'Selectionnez Division..']) !!}</td>
                </tr>
                <tr>
                <td>{{Form::label('type', 'Type')}} </td><td>{!! Form::select('type', 
                  ["Instruction" => "Instruction","Requete" => "Requete","Demande audience" => "Demande audience"], 
                            null,['placeholder' => 'Selectionnez Type..']) !!}</td>

                <td>{{Form::label('etat', 'Etat')}} </td><td>{!! Form::select('etat', 
                  ["en cours" => "en cours","traité" => "traité"], 
                            null,['placeholder' => 'Selectionnez Etat..']) !!}</td>
                <td>{{Form::submit('Valider',array('class' => 'button'))}}</td>
                </tr>
                <br />
                </table>
                {{ Form::close() }}
               
                
                <div class="tablebox">
                  @if (Session::has('response'))
                      @if (count(Session::get('response'))===0)
                      <tr><td>Aucun élément trouvé</td></tr>
                      @else
                      @php
                       $mps = Session::get('response');
                      @endphp
                      @include('layouts.mps.searchMpTable',$mps)
                      @endif
                    @else
                  @include('layouts.mps.mptable',$mps)
                  @endif
                </div>
                <div class="clear"></div>
                @endsection