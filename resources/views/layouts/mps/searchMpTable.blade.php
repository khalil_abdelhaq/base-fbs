

<a href="{{route('mps.exportsearch')}}" value="Export" ><img width="50" height="50" src="img/iconeexcel.png" alt="logo"/></a>

                  <table border="1|0">
                      <thead>
                          <tr>
                          <th colspan="1" rowspan="2">Type</th>
                            <th colspan="5">Référence Bureau d'ordre</th>
                            <th colspan="5">Référence Cabinet</th>
                            <th colspan="2"></th>
                          </tr>
                          <tr>
                          <th>N° ENVOI</th>
                          <th> Date envoie</th>
                          <th> N° Arrivé</th>
                          <th>Date arrivé</th>
                          <th>Emetteur</th>

                          <th>Date VISA</th>
                          <th>Objet</th>
                          <th>Division</th>
                          <th>Instructions</th>
                          <th>date limit</th>
                          <th>Etat</th>
                          <th>Actions</th>
                          </tr>
                      </thead>
                      <tbody>
                      @foreach ($mps as $mp)
                      <tr class="row0">
                      <td>{{$mp->type}}</td>
                      <td>{{$mp->num_envoi}}</td>
                      <td>{{$mp->date_envoie}}</td>
                      <td>{{$mp->num_arrive}}</td>
                      <td>{{$mp->date_arrive}}</td>
                      <td>{{$mp->emetteur}}</td>

                      <td>{{$mp->date_visa}}</td>
                      <td>{{$mp->objet}}</td>
                      <td>{{$mp->divisionconcernee->acronyme}}</td>
                      <td>{{$mp->instructions}}</td>
                      <td>{{$mp->date_limit}}</td>
                      <td>{{$mp->etat}}</td>

                      <td>
                      <a  href="#" title=""><input type="button" class="fa view" value="&#xf06e;"/></a>
                      <a  href="{{ route('mps.edit',$mp->id)}}" title=""><input type="button" class="fa view" value="&#xf044;"/></a>
                      {!! Form::open(['method' => 'DELETE','route' => ['mps.destroy', $mp->id],'style'=>'display:inline','onsubmit' => 'return confirmDeletemps()']) !!}
                            <input type="submit" class="fa delete" value="&#xf1f8;" />
                      {!! Form::close() !!}
                      </td>
                      </tr>
                      @endforeach                   
                      </tbody>
                  </table>
                