@extends('layouts.app')
@section('content')  
                    
            <h3><span class="title">Ajouter une instance</span><span class="underlined">&nbsp;</span></h3>  
            @if (Session::has('message'))
                <div class="success">
                    {!! Session::get('message') !!}
                </div>
            @endif       
            {{ Form::model($dossier, array('route' => array('dossiers.update', $dossier->id), 'method' => 'PUT')) }}
                <fieldset class="new_folder">
                <legend>Veuillez remplir les champs</legend>
    				<table>
                    <tr>
                    <td>{{Form::label('objet', 'Intitulé')}}</td>
                    <td> {{Form::text('objet')}}</td>
                    <td>
                    @if ($errors->has('objet'))
                    <span class="error"> {{ $errors->first('objet') }}</span>
                    @endif
                    </td>
                    </tr>
                    <tr>
                       <td> {{ Form::hidden('division_id',Auth::user()->division_id) }} </td><td></td>
                    </tr>
                    <tr>
                        <td>{{Form::label('dvi', 'Divisions impliquées')}}</td>
                        <td>
                        {!! Form::select('di[]', 
                            $divisions, 
                            null, 
                            ['multiple' => 'multiple']) !!}
                        </td>
                        <td><span></span></td>
                      </tr>
                      <tr>    
                         <td> {{Form::label('type_id', 'Type du dossier')}}</td>
                          <td>
                          {!! Form::select('type_id', 
                            $types, 
                            null,[]) !!}
                          </td>
                          <td>
                          @if ($errors->has('type_id'))
                          <span class="error">{{ $errors->first('type_id') }}</span>
                          @endif
                          </td>
                    </tr>
                       <tr>  
                       <td> 
                          {{Form::label('priorite', 'Priorité')}}
                        </td>  
                        <td>
                        {{Form::select('priorite', array('Critique' => 'Critique', 'Elevé' => 'Elevé'))}}
                        </td>
                        <td>
                        @if ($errors->has('priorite'))
                        <span class="error">{{ $errors->first('priorite') }}</span>
                        @endif
                        </td>
                        </tr>

                        <tr>  
                       <td> 
                          {{Form::label('Etat', 'Etat dossier')}}
                        </td>  
                        <td>
                        {!! Form::select('etat_id', 
                            $etats, 
                            null,[]) !!}
                        </td>
                        <td>
                        @if ($errors->has('etat_id'))
                        <span class="error">{{ $errors->first('etat_id') }}</span>
                        @endif
                        </td>
                        </tr>
                        <tr>
                        <td>
                        {{Form::label('date_debut', 'Date début')}}
                        {{Form::date('date_debut')}}
                        </td>
                        <td>
                        {{Form::label('date_fin', 'Date Fin')}}
                        {{Form::date('date_fin')}}
                        </td>
                        <td>
                          @if ($errors->has('date_fin'))
                          <span class="error">{{ $errors->first('date_fin') }}</span>
                          @endif
                          </td>
                        </tr>
                        <tr>
                        <td>
        				{{Form::label('description', 'Description')}}
                        </td> 
                        <td>
        				{{ Form::textarea('description') }}
                        </td>
                        <td>
                        @if ($errors->has('description'))
                        <span class="error">{{ $errors->first('description') }}</span>
                        @endif
                        </td>
        				</tr>
                        <tr>
                        <td></td>
                        <td>
                        {{Form::submit('Valider',array('class' => 'button'))}}
                        </td>
                        <td></td>
                        </tr>
        				
                        </table>
                </fieldset>
				{{ Form::close() }}
                <!-- end block example form -->
                <div class="clear"></div>           
                @endsection