@extends('layouts.app')
@section('content')
<script type="text/javascript">
    window.onload = function() {
    var options = {
        exportEnabled: true,
        animationEnabled: true,
        title:{
            text: "Dossiers"
        },
        legend:{
            horizontalAlign: "right",
            verticalAlign: "center"
        },
        data: [{
            type: "pie",
            showInLegend: true,
            toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
            indexLabel: "{name}",
            legendText: "{name} (#percent%)",
            indexLabelPlacement: "inside",
            dataPoints: [              
                @foreach ($etatdossiers as $state)
            { y: {{$state->count}}, name: "{{$state->intitule}}" },
            @endforeach
            ]
        }]
    };
    $("#chartContainer").CanvasJSChart(options);

    var optionsGraphe = {
	animationEnabled: true,
	title: {
		text: "Dossier par division"
	},
	axisY: {
		title: "Nombre de dossiers",
		suffix: "",
		includeZero: false
	},
	axisX: {
		title: "Divisions"
	},
	data: [{
		type: "column",
		yValueFormatString: "#,##0.0#"%"",
		dataPoints: [
            @foreach ($divisions as $division)
            { label: "{{$division->acronyme}}", y: {{$division->count}} },
            @endforeach	
		]
	}]
};
$("#GrapheContainer").CanvasJSChart(optionsGraphe);
    
    }
    
    </script>            
            <div id="chartContainer" style="height: 370px; width: 40%;display: inline-block;"></div>
           
            <div id="GrapheContainer" style="height: 370px; width: 40%;display: inline-block;"></div>
      <!-- end div content -->
@endsection