@extends('layouts.app')
@section('content')               
               <!-- block Fade in/out Message box -->
            <!--
                <h3><span class="title">Fade in/out Message box</span><span class="underlined">&nbsp;</span></h3>
                <input type="button" class="button" value="Click here to show message box" name="open_msgbox" id="open_msgbox" />
                <div class="msgbox" id="msgbox1">
                    <div class="icon"><img src="img/icons/alert.gif" alt="" title="" /></div>
                    <div class="text">
                        This block is used to display messages ( e.g. Your Page was successfully updated. )
                        <br />
                        click icon to close this message box!<br />
                    </div>
                    <div class="close"><a href="#" id="close_msgbox" title="Close message box"><img src="img/icons/icon_minus.gif" alt="" title="" /></a></div>
                    <div class="clear"></div>
                </div>
                <!-- end block Fade in/out Message box -->

           
                
                <!-- block example form -->                

                @if (Session::get('response'))
                <div class="success">
                    {!! Session::get('response')['message'] !!}
                </div> 
                <fieldset class="new_folder">
                <legend>Récapitulatif</legend>
                <div>
                <table>
                <tr>
                <td>Objet</td><td><strong>{!!Session::get('response')['data']['objet'] !!}</strong></td>
                </tr>
                <tr>
                <td>Priorité</td><td><strong>{!!Session::get('response')['data']['priorite'] !!}</strong></td>
                </tr>
                <tr>
                <td>Date début</td><td><strong>{!!Session::get('response')['data']['date_debut'] !!}</strong></td>
                </tr>
                <tr>
                <td>Date fin</td><td><strong>{!!Session::get('response')['data']['date_fin'] !!}</strong></td>
                </tr>
                <tr>
                <td>Description</td><td><strong>{!!Session::get('response')['data']['description'] !!}</strong></td>
                </tr>
                </table>
                </div>
                </fieldset>
                    @else        
            <h3><span class="title">Ajouter une instance</span><span class="underlined">&nbsp;</span></h3>         
				{{ Form::open(['route' => 'dossiers.store']) }}
                <fieldset class="new_folder">
                <legend>Veuillez remplir les champs</legend>
    				<table>
                    <tr>
                    <td>{{Form::label('objet', 'Intitulé')}}</td>
                    <td> {{Form::text('objet')}}</td>
                    <td>
                    @if ($errors->has('objet'))
                    <span class="error"> {{ $errors->first('objet') }}</span>
                    @endif
                    </td>
                    </tr>
                    <tr>
                       <td> {{ Form::hidden('division_id',Auth::user()->division_id) }} </td><td></td>
                    </tr>
                    <tr>
                        <td>{{Form::label('dvi', 'Divisions impliquées')}}</td>
                        <td>
                        {!! Form::select('di[]', 
                            $divisions, 
                            null, 
                            ['multiple' => 'multiple']) !!}
                        </select>
                        </td>
                        <td><span></span></td>
                      </tr>
                      <tr>    
                         <td> {{Form::label('type_id', 'Type du dossier')}}</td>
                          <td>
                          {!! Form::select('type_id', 
                            $types, 
                            null,[]) !!}
                          </td>
                          <td>
                          @if ($errors->has('type_id'))
                          <span class="error">{{ $errors->first('type_id') }}</span>
                          @endif
                          </td>
                    </tr>
                       <tr>  
                       <td> 
                          {{Form::label('priorite', 'Priorité')}}
                        </td>  
                        <td>
                        {{Form::select('priorite', array('Critique' => 'Critique', 'Elevé' => 'Elevé','Normal' => 'Normal'))}}
                        </td>
                        <td>
                        @if ($errors->has('priorite'))
                        <span class="error">{{ $errors->first('priorite') }}</span>
                        @endif
                        </td>
                        </tr>
                        <tr>
                        <td>
                        {{Form::label('date_debut', 'Date début')}}
                        {{Form::date('date_debut', \Carbon\Carbon::now())}}
                        </td>
                        <td>
                        {{Form::label('date_fin', 'Date Fin')}}
                        {{Form::date('date_fin', \Carbon\Carbon::now())}}
                        </td>
                        <td>
                          @if ($errors->has('date_fin'))
                          <span class="error">{{ $errors->first('date_fin') }}</span>
                          @endif
                          </td>
                        </tr>
                        <tr>
                        <td>
        				{{Form::label('description', 'Description')}}
                        </td> 
                        <td>
        				{{ Form::textarea('description') }}
                        </td>
                        <td>
                        @if ($errors->has('description'))
                        <span class="error">{{ $errors->first('description') }}</span>
                        @endif
                        </td>
        				</tr>
                        <tr>
                        <td></td>
                        <td>
                        {{Form::submit('Valider',array('class' => 'button'))}}
                        </td>
                        <td></td>
                        </tr>
        				
                        </table>
                </fieldset>
				{{ Form::close() }}
                <!-- end block example form -->


                <div class="clear"></div>
                @endif
                @endsection