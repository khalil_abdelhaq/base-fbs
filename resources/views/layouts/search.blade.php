@extends('layouts.app')
@section('content')
<h3><span class="title">Trouver un dossier</span><span class="underlined">&nbsp;</span></h3>
            {{ Form::open(['route' => 'dossiers.search']) }}
                <fieldset>
                <legend>Critères de recherche</legend>
    				<table>
                    <tr>  
                    <td>              
                   {{Form::label('mots', 'Mots clé')}}
                   </td>
                   <td>
                    {{Form::text('mots')}}
                  </td>

                  <td>
                  @if (Auth::user()->isSuperAdmin())
                  {{Form::label('division', 'division')}}
                  </td>
                  <td>    
                  {!! Form::select('division_id', 
                            $divisions, 
                            null,['placeholder' => 'Selectionnez Division..']) !!}
                    @endif
                   </td> 
                  </tr>
                  <tr>
                  <td>
                  {{Form::label('type_id', 'Type dossier')}}
                  </td>
                  <td>    
                  {!! Form::select('type_id', 
                            $types, 
                            null,['placeholder' => 'Selectionnez Type..']) !!}
                   </td>     
                     </tr>   
                        <tr>
                        <td>
                        {{Form::label('date_debut', 'Date début')}}
                        </td>
                        <td>
                        {{Form::date('date_debut')}}
                        </td>
                        <td>
                        {{Form::label('date_fin', 'Date Fin')}}
                        </td>
                        <td>
                        {{Form::date('date_fin')}}
                        </td>
                        </tr>
        				<tr>
                        <td>
                        {{Form::submit('Valider',array('class' => 'button'))}}
                        </td>
                        </tr>
    				</table>
                  
                </fieldset>
                    {{ Form::close() }}
                <!-- end block example form -->


                <div class="clear"></div>
            
                <h3><span class="title">Résultat de la recherche</span><span class="underlined">&nbsp;</span></h3>
                <div class="tablebox">
                  <table>
                      <thead>
                      <tr>
                            <th>Intitulé</th>
                            <th>Type</th>
                            <th>Divisions impliquées</th>
                            <th>Priorité</th>
                            <th>Etat</th>
                            <th>Description</th>
                            <th class="action">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      @if (Session::has('response'))
                      @if (count(Session::get('response'))===0)
                      <tr><td>Aucun élément trouvé</td></tr>
                      @else
                      @foreach (Session::get('response') as $dossier)
                      <tr class="row0">
                            <td>{{$dossier->objet}}</td>
                            <td>{{$dossier->type->intitule}}</td>
                            <td>
                            @if(count($dossier->divisionsimpliquees)===0)
                            Aucune division impliquée
                            @else
                            @foreach ($dossier->divisionsimpliquees as $division)
                            <span>{{ $division->acronyme }}</span>
                            @endforeach
                            @endif
                            </td>
                            <td>{{$dossier->priorite}}</td>                          
                            <td>{{$dossier->etat->intitule}}</td>
                            <td>{{$dossier->description}}</td>
                            <td class="action">
                            <a  href="{{ route('dossiers.show',$dossier->id)}}" title=""><input type="button" class="fa view" value="&#xf06e;"/></a>
                            <a href="{{ route('dossiers.edit',$dossier->id)}}" title=""><input type="button" class="fa edit" value="&#xf044;"/></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['dossiers.destroy', $dossier->id],'style'=>'display:inline']) !!}
                            <input type="submit" class="fa delete" value="&#xf1f8;" />
                            {!! Form::close() !!}
                                   
                          </td>
                          </tr>     
                      @endforeach
                      @endif                        
                      </tbody>
                  </table>
                  <table>
                          <tr>
                              <td>                       
                              <b> Total: {{Session::get('response')->total()}} dossiers trouvés</b>
                              </td>
                              <td class="pagination">
                              {{ Session::get('response')->links() }}
                              </td>
                          </tr>
                  </table>
                  @endif 
                </div>
                <!-- end block example table -->
@endsection
              