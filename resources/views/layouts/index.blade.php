@extends('layouts.app')
@section('content')
<h3><span class="title">Les dossiers en instance</span><span class="underlined">&nbsp;</span></h3>
                
                <div class="tablebox">
                  <table border="1|0">
                      <thead>
                          <tr>
                            <th>Intitulé</th>
                            <th>Division concernée</th>
                            <th>Divisions impliquées</th>
                            <th>Priorité</th>
                            <th>Etat</th>
                            <th>Description</th>
                            <th class="action">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                      @foreach ($dossiers as $dossier)
                      <tr class="row0">
                            <td>{{$dossier->objet}}</td>
                            <td>{{$dossier->divisionconcernee->acronyme}}</td>
                            <td>
                            @if(count($dossier->divisionsimpliquees)===0)
                            Aucune division impliquée
                            @else
                            @foreach ($dossier->divisionsimpliquees as $division)
                            <span>{{ $division->acronyme }}</span>
                            @endforeach
                            @endif
                            </td>
                            <td>{{$dossier->priorite}}</td>                          
                            <td>{{$dossier->etat->intitule}}</td>
                            <td>{{$dossier->description}}</td>
                            <td class="action">
                            <a  href="{{ route('dossiers.show',$dossier->id)}}" title=""><input type="button" class="fa view" value="&#xf06e;"/></a>
                            <a href="{{ route('dossiers.edit',$dossier->id)}}" title=""><input type="button" class="fa edit" value="&#xf044;"/></a>
                            {!! Form::open(['method' => 'DELETE','route' => ['dossiers.destroy', $dossier->id],'style'=>'display:inline','onsubmit' => 'return confirmDelete()']) !!}
                            <input type="submit" class="fa delete" value="&#xf1f8;" />
                            {!! Form::close() !!}
                                   
                          </td>
                          </tr>     
                        @endforeach                                      
                      </tbody>
                  </table>
                  <table>
                          <tr>
                              <td>                       
                              <b> Total: {{$dossiers->total()}} dossiers trouvés</b>
                              </td>
                              <td class="pagination">
                              {{ $dossiers->links() }}
                              </td>
                          </tr>
                  </table>
                </div>
                <div class="clear"></div>
                @endsection