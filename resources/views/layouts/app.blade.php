<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/img/icons/logo_interieur.ico')}}" />
  <title>Province Fquih ben Saleh</title>
  
  
  <link type="text/css" href="{{ asset('/css/style.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css')}}" />

  <!--[if lte IE 6]>
    <link type="text/css" href="./css/style_ie6.css" rel="stylesheet" />
  <![endif]-->
  <script type="text/javascript" src="{{ asset('/js/jquery-1.11.1.min.js')}}"></script> 
  <script type="text/javascript" src="{{ asset('/js/script.js')}}"></script>
<script type="text/javascript" src="{{ asset('/js/jquery.canvasjs.min.js')}}"></script>
</head>

<body>
<div id="page">

  <!-- header -->
  <div id="header">
      <div id="logo"><h1><a href="#" title="Your site name"><img style="height:100px" src="{{ asset('/img/logo_interieur.png')}}" alt="logo"/></a></h1></div>
      <div id="div_header"><img src="{{ asset('img/header.png')}}" /></div>
      <div id="quicklink"><i class="fa fa-user fa-lg" aria-hidden="true"></i>
 | <a href="{{ route('login') }}">{{ Auth::user()->name }}</a> | <a href="{{ url('/logout') }}"
    onclick="event.preventDefault();
             document.getElementById('logout-form').submit();" title="logout"><i class="fa fa-power-off fa-lg" aria-hidden="true"></i>
</a>
<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
</div>
  </div>
  <!-- end header -->


  <!-- main menu -->
  <div id="mainmenu">
      <ul>
      <!--
          <li class="item"><a href="{{url('/folder')}}"><span class="item-icon"><i class="fa fa-folder fa-2x"></i></span><span class="item-text">DOSSIERS</span></a></li>
          <li class="item"><a href="{{ route('dossiers.create') }}"><span class="item-icon"><i class="fa fa-folder-open fa-2x"></i></span><span class="item-text">NOUVEAU</span></a></li>
          <li class="item"><a href="{{ url('/search') }}"><span class="item-icon"><i class="fa fa-search fa-2x"></i></span><span class="item-text">CHERCHER</span></a></li>

          @if (Auth::user()->isSuperAdmin())
          <li class="item"><a href="{{ url('/statistics') }}"><span class="item-icon"><i class="fa fa-pie-chart fa-2x"></i></span><span class="item-text">STATISTIQUES</span></a></li>
          @endif
          -->
          <li class="item"><a href="{{ url('/mps') }}"><span class="item-icon"><i class="fa fa-edit fa-2x"></i></span><span class="item-text">INSTRUCTIONS</span></a></li>
          <li class="item"><a href="{{ route('mps.create') }}"><span class="item-icon"><i class="fa fa-folder-open fa-2x"></i></span><span class="item-text">NOUVEAU</span></a></li>
      </ul>
  </div>
  <!-- end mainmenu -->
<!--
    <div id="submenu">
        <ul>
            <li><a href="#" title="">Manager</a></li>
            <li><a href="#" title="" class="active">Advanced search</a></li>
            <li><a href="#" title="">Options</a></li>
            <li><a href="#" title="">Daily Reports</a></li>
        </ul>
        <div class="clear"></div>
    </div>
-->
  <!-- content -->
      <div id="content">
          @yield('content')              
      </div>
      <!-- end div content -->

  <!-- Footer -->
  <div id="footer">
      <ul>
          <li>&copy;2018 <a href="#" title="">Gestion des MPs</a>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
          <li>Powered by <a href="#" title="">DSIC FBS</a></li>
      </ul>
  </div>

</div>
<!-- end div page -->
</body>

</html>