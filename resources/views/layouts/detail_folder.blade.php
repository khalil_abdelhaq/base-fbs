@extends('layouts.app')
@section('content')
<script type="text/javascript">
$(function() {
  
  // contact form animations
  $('#addevenement').click(function() {
    $('#evenementForm').fadeToggle();
  })
  $(document).mouseup(function (e) {
    var container = $("#evenementForm");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.fadeOut();
    }
  });
  
});
</script>
<div id="c-mask" class="c-mask is-active"></div>
                <fieldset>
                <legend>Détails dossier</legend>
    				<p>
            <table>
            <tr>
            <td>
        				<label>Objet</label>
            </td>
            <td>
        				<input name="dname" value="{{$dossier->objet}}" type="text" size="30" disabled="true"/>
            </td>    
            </tr>   
            <tr>
            <td><label>Division concernée</label></td>
            <td> @foreach($divisionimpliques as $division) 
                <input name="{{$division->intitule}}" value="{{$division->acronyme}}" type="text" size="30" />
                @endforeach</td>
            </tr> 

            <tr>
            <td><label>Date début</label></td>
            <td><input name="ddebut" value="{{$dossier->date_debut}}" type="text" size="30" disabled="true"/></td>
            </tr>

            <tr>
            <td><label>Date Fin</label></td>
            <td><input name="dfin" value="{{$dossier->date_fin}}" type="text" size="30" disabled="true"/></td>
            </tr>
                
            <tr>
            <td>	<label>Description</label></td>
            <td>	<textarea rows="5" cols="5" value="">{{$dossier->description}}</textarea></td>
            </tr>    
                </table>
    				</p>
            <br />
                </fieldset>
                <!--  Liste événement -->
                <fieldset>
                <legend>Historique des évenements</legend>
                <a class="add fa" id="addevenement" href="#" title="">&#xf067 Ajouter un évenement</a>
                <div class="evenementsbox">
                
                @if (count($evenements) === 0)
                Aucun évenement
                @else
                  <table style="width:70%">
                      <thead>
                          <tr>                    
                            <th width="20%">objet</th>
                            <th width="30%">Date d'évenement</th>
                            <th width="50%">Remarques</th>
                          </tr>
                          @foreach($evenements as $evenement) 
                          <tr>                        
                            <td>{{$evenement->intitule}}</td>
                            <td>{{$evenement->created_at}}</td>
                            <td>{{$evenement->remarques}}</td>
                          </tr>
                          @endforeach
                      </thead>
                      <tbody>                          
                      </tbody>
                  </table>
                  @endif
                </div>
</fieldset>

<!-- popup evenement -->

<div id="evenementForm">  
  {{ Form::open(['route' => 'evenements.store']) }}
    <table>
    <tr>
    <td>{{Form::label('intitule', 'Intitulé')}}</td>
    <td>{{Form::text('intitule')}}</td>
    <td> @if ($errors->has('type_id'))
      <span class="error">{{ $errors->first('intitule') }}</span>
     @endif
     </td>
    </tr>
    <tr>
    <td>{{Form::label('remarques', 'Remarques')}}</td>
    <td>{{ Form::textarea('remarques') }}</td>
    <td>
    <td> @if ($errors->has('reamrques'))
      <span class="error">{{ $errors->first('remarques') }}</span>
     @endif
     </td>
    </td>
    </tr>
    <tr>
      <td> {{ Form::hidden('dossier_id',$dossier->id) }} </td>
      
    </tr>
    </table>
    {{Form::submit('Valider',array('class' => 'button'))}}
    {{ Form::close() }}
</div>

@endsection